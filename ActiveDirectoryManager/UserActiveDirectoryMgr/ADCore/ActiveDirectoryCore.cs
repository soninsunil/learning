﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using UserActiveDirectory.Utility;

namespace UserActiveDirectory.ADCore
{
    //TODO: do we really need singleton here or normal static class will do the work
    public sealed class ActiveDirectoryCore
    {
        private static readonly ActiveDirectoryCore instance = new ActiveDirectoryCore();

        static ActiveDirectoryCore()
        {
            DomainName = ConfigurationManager.AppSettings["DomainName"];
        }

        private ActiveDirectoryCore()
        {
        }

        private static readonly string DomainName;
        public static ActiveDirectoryCore Instance
        {
            get
            {
                return instance;
            }
        }

        public bool IsUserMember(string userName, string distributionList)
        {
            try
            {
                using (var pc = new PrincipalContext(ContextType.Domain, DomainName))
                {
                    using (var gp = GroupPrincipal.FindByIdentity(pc, distributionList))
                    {
                        if (gp != null)
                        {
                            using (var up = UserPrincipal.FindByIdentity(pc, userName))
                            {
                                if (up != null)
                                    return up.IsMemberOf(gp);
                                else
                                    throw new Exception($"User {userName} is not valid.");
                            }
                        }
                        else
                            throw new Exception($"DL {distributionList} is not valid.");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool AddUserToGroup(string userId, string groupName)
        {
            bool isAdded = false;
            try
            {
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, DomainName))
                {
                    GroupPrincipal group = GroupPrincipal.FindByIdentity(pc, groupName);
                    group.Members.Add(pc, IdentityType.UserPrincipalName, userId);
                    group.Save();
                    isAdded = true;
                }
            }
            catch (DirectoryServicesCOMException)
            {
                //doSomething with E.Message.ToString(); 
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return isAdded;
        }

        public void RemoveUserFromGroup(string userId, string groupName)
        {
            try
            {
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, DomainName))
                {
                    GroupPrincipal group = GroupPrincipal.FindByIdentity(pc, groupName);
                    group.Members.Remove(pc, IdentityType.UserPrincipalName, userId);
                    group.Save();
                }
            }
            catch (DirectoryServicesCOMException)
            {
                //doSomething with E.Message.ToString(); 
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetUsersFromGroup(string sGroupName)
        {
            DataTable dt;
            DataRow dr;

            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, DomainName))
            {
                GroupPrincipal group = GroupPrincipal.FindByIdentity(pc, sGroupName);

                if (group == null)
                {
                    throw new Exception($"No DL found with the name '{sGroupName}'");
                }

                dt = new DataTable();
                dt.Columns.Add("Emp No",typeof(string));
                dt.Columns.Add("Emp Mail ID", typeof(string));
                dt.Columns.Add("Location", typeof(string));
                dt.Columns.Add("Country", typeof(string));
                dt.Columns.Add("Role", typeof(string));
                dt.Columns.Add("DL", typeof(string));

                foreach (Principal p in group.Members)
                {
                    dr = dt.NewRow();

                    if (p.StructuralObjectClass.ToString() == "user")
                    {
                        string strCompany, strTitle;
                        using (var foundUser = UserPrincipal.FindByIdentity(pc, IdentityType.SamAccountName, p.SamAccountName))
                        {
                            DirectoryEntry directoryEntry = foundUser.GetUnderlyingObject() as DirectoryEntry;
                            strCompany = directoryEntry.Properties["company"].Value.ToString();
                            strTitle = directoryEntry.Properties["title"].Value.ToString();
                        }

                        dr["Emp No"] = strCompany;
                        dr["Role"] = strTitle;

                        dr["Emp Mail ID"] = p.SamAccountName;

                        var temp = p.DistinguishedName.GetList();
                        dr["Location"] = temp[4].Substring(3);
                        dr["Country"] = temp[5].Substring(3);
                        dr["DL"] = group.Name;
                    }
                }
                return dt;
            }
        }

        public string GetUserDetail(string userName)
        {
            using (var domainContext = new PrincipalContext(ContextType.Domain, DomainName))
            {
                using (var foundUser = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, userName))
                {
                    if (foundUser != null)
                    {
                        try
                        {
                            DirectoryEntry directoryEntry = foundUser.GetUnderlyingObject() as DirectoryEntry;
                            // string strdepartment = directoryEntry.Properties["department"].Value.ToString();
                            //  string strcompany = directoryEntry.Properties["company"].Value.ToString();
                            //many details
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
            }
            return "";
        }


        public bool GetUserMemberShip(string userName, string distributionListName)
        {
            bool isUserMember = false;
            using (var domainContext = new PrincipalContext(ContextType.Domain, DomainName))
            {
                using (var foundUser = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, userName))
                {
                    if (foundUser != null)
                    {
                        try
                        {
                            isUserMember = foundUser.IsMemberOf(domainContext, IdentityType.SamAccountName, userName);
                            // DirectoryEntry directoryEntry = foundUser.GetUnderlyingObject() as DirectoryEntry;
                            // string strdepartment = directoryEntry.Properties["department"].Value.ToString();
                            //  string strcompany = directoryEntry.Properties["company"].Value.ToString();
                            //many details
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
            }
            return isUserMember;
        }

        public string[] GetGroupNames(string userName)
        {
            List<string> result = new List<string>();

            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, DomainName))
            {
                using (PrincipalSearchResult<Principal> src = UserPrincipal.FindByIdentity(pc, userName).GetGroups(pc))
                {
                    src.ToList().ForEach(sr => result.Add(sr.SamAccountName));
                }
            }

            return result.ToArray();
        }

        public void GetAD(string userName)
        {
            using (PrincipalContext context = new PrincipalContext(ContextType.Domain))
            {
                using (UserPrincipal user = new UserPrincipal(context))
                {
                    user.SamAccountName = userName;
                    using (PrincipalSearcher searcher = new PrincipalSearcher(user))
                    {

                        foreach (Principal result in searcher.FindAll())
                        {
                            DirectoryEntry de = result.GetUnderlyingObject() as DirectoryEntry;
                            for (int i = 0; i < de.Properties["memberOf"].Count; i++)
                            {
                                Console.WriteLine("Group name:{0}", de.Properties["memberOf"][i]);
                            }

                            //var tt = (from object o in de.Properties["memberOf"]
                            //          select o).ToList();

                            //foreach (var group in tt)
                            //{
                            //    Console.WriteLine("Group name:{0}", group.ToString());
                            //}

                        }
                    }
                }
            }
        }

        public List<string> GetListOfDLforUser(string userName)
        {
            // create the "context" in which to operate - your domain here, 
            // as the old-style NetBIOS domain, and the container where to operate in
            //PrincipalContext ctx = new PrincipalContext(ContextType.Domain, DomainName, "cn=Distribution Group,dc=YourDomain,dc=local");

            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, DomainName, "cn=Distribution Group,dc=YourDomain,dc=local");

            // define a "prototype" - an example of what you're searching for
            // Here: just a simple GroupPrincipal - you want all groups
            GroupPrincipal prototype = new GroupPrincipal(ctx)
            {
                //Todo: Test this with user
                SamAccountName = userName
            };
            // define a PrincipalSearcher to find those principals that match your prototype
            PrincipalSearcher searcher = new PrincipalSearcher(prototype);

            // define a list of strings to hold the group names        
            List<string> groupNames = new List<string>();

            // iterate over the result of the .FindAll() call
            foreach (var gp in searcher.FindAll())
            {
                // cast result to GroupPrincipal
                GroupPrincipal group = gp as GroupPrincipal;

                // if everything - grab the group's name and put it into the list
                if (group != null)
                {
                    groupNames.Add(group.Name);
                }
            }
            return groupNames;
        }

        public DateTime? GetLastPasswordSet(string domain, string userName)
        {
            using (var context = new PrincipalContext(ContextType.Domain, domain))
            {
                var userPrincipal = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName);
                return userPrincipal.LastPasswordSet;
            }
        }


    }
}
