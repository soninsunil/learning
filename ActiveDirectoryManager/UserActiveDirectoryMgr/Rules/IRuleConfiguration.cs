﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserActiveDirectory.Rules
{
    public interface IRuleConfiguration
    {
        string GetDLName();
    }
}
