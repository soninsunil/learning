﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserActiveDirectory.Excel;
using UserActiveDirectory.Utility;

namespace UserActiveDirectory.Rules
{
    public static class CustomDLName
    {
        private static readonly DataTable location;
        private static readonly DataTable role;
        private static readonly List<string> UnitSubNames;

        static CustomDLName()
        {
            location = Reader.ReadExcel(ConfigurationManager.AppSettings["LocationXLS"]);
            role = Reader.ReadExcel(ConfigurationManager.AppSettings["RoleXLS"]);
            UnitSubNames = ConfigurationManager.AppSettings["DUSubUnit"].GetList();
        }

        public static string GetLocationDLName(string UnitName, string empLocation, bool isOnsite = false)
        {
            string mainUnit = ConfigurationManager.AppSettings["PUMainUnit"];

            if (isOnsite && UnitSubNames.Contains(UnitName.Split('-')[1]))
            {
                string subUnit = ConfigurationManager.AppSettings["PUSecUnit"];

                return string.Format("{0}.{1}.{2}", subUnit, UnitName.Split('-')[1], UnitName.Split('-')[1] == "OSDM" ? "ONS" : "ONST");
            }
            else
            {
                string locationFilter = UnitSubNames.Contains(UnitName.Split('-')[1]) ?
                                            string.Format("[Emp_Location] = '{0}' AND [Emp_DU] = '{1}'", empLocation, UnitName.Split('-')[1]) :
                                            string.Format("[Emp_Location] = '{0}' AND ([Emp_DU] = '' OR [Emp_DU] IS NULL)", empLocation);

                var locationDLName = location.Select(locationFilter);

                return locationDLName.Select(a => a.ItemArray[2].ToString()).FirstOrDefault();
            }
        }

        public static string GetRoleDLName(string UnitName, string roleName)
        {
            string roleFilter = UnitSubNames.Contains(UnitName.Split('-')[1]) ?
                                    string.Format("[Emp_Role] = '{0}' AND [Emp_DU] = '{1}'", roleName, UnitName.Split('-')[1]) :
                                    string.Format("[Emp_Role] = '{0}' AND ([Emp_DU ='' OR [Emp_DU] is NULL )", roleName);

            var roleDLName = role.Select(roleFilter);

            return roleDLName.Select(a => a.ItemArray[2].ToString()).FirstOrDefault();
        }

    }
}
