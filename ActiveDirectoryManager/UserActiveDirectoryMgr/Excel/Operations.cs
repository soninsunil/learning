﻿using ExcelNameSpace = Microsoft.Office.Interop.Excel;
using System;
using System.Data.OleDb;
using System.IO;
using System.Windows.Forms;
using System.Data;

namespace UserActiveDirectory.Excel
{
    public static class Operations
    {
        #region Export Data to Excel from DataGridView
        public static void ExportToExcel(DataGridView dataGrid, string filePath, string workSheetName = "")
        {
            ExcelNameSpace._Application excel = new ExcelNameSpace.Application();
            ExcelNameSpace._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            ExcelNameSpace._Worksheet worksheet = null;

            try
            {

                worksheet = workbook.ActiveSheet;

                if (string.IsNullOrEmpty(workSheetName))
                    worksheet.Name = "ExportedFromDatGrid";
                else worksheet.Name = workSheetName;

                int cellRowIndex = 1;
                int cellColumnIndex = 1;

                //Loop through each row and read value from each column. 
                for (int i = -1; i < dataGrid.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < dataGrid.Columns.Count; j++)
                    {
                        // Excel index starts from 1,1. As first Row would have the Column headers, adding a condition check. 
                        if (cellRowIndex == 1)
                        {
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = dataGrid.Columns[j].HeaderText;
                        }
                        else
                        {
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = dataGrid.Rows[i].Cells[j].Value.ToString();
                        }
                        cellColumnIndex++;
                    }
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }

                workbook.SaveAs(filePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                excel.Quit();
                workbook = null;
                excel = null;
            }
        }
        #endregion

        #region Import from Excel to DataGrid
        public static DataTable ImportExcel(string pathName, string sheetName = "", bool IsImportHeader = true)
        {
            try
            {

                // string pathName = openFileDialog1.FileName;
                DataTable tbContainer = new DataTable();
                string strConn = string.Empty;

                FileInfo file = new FileInfo(pathName);
                if (!file.Exists) { throw new Exception("Error, file doesn't exists!"); }

                string extension = file.Extension;
                switch (extension)
                {
                    case ".xls":
                        strConn = $"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={pathName};Extended Properties='Excel 8.0;HDR={(IsImportHeader ? "Yes" : "No")};IMEX=1;'";
                        break;
                    case ".xlsx":
                        strConn = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={pathName};Extended Properties='Excel 12.0;HDR={(IsImportHeader ? "Yes" : "No")};IMEX=1;'";
                        break;
                    default:
                        strConn = $"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={pathName};Extended Properties='Excel 8.0;HDR={(IsImportHeader ? "Yes" : "No")};IMEX=1;'";
                        break;
                }
                if (string.IsNullOrEmpty(sheetName))
                {
                    using (OleDbConnection con = new OleDbConnection(strConn))
                    {
                        using (OleDbCommand cmd = new OleDbCommand())
                        {
                            cmd.Connection = con;
                            con.Open();
                            DataTable dtExcelSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                            con.Close();
                        }
                    }
                }

                //Read Data from the First Sheet.
                using (OleDbConnection con = new OleDbConnection(strConn))
                {
                    using (OleDbCommand cmd = new OleDbCommand())
                    {
                        using (OleDbDataAdapter oda = new OleDbDataAdapter())
                        {
                            cmd.CommandText = "SELECT * From [" + sheetName + "]";
                            cmd.Connection = con;
                            con.Open();
                            oda.SelectCommand = cmd;
                            oda.Fill(tbContainer);
                            con.Close();
                            return tbContainer;
                        }
                    }
                }

                //  OleDbConnection cnnxls = new OleDbConnection(strConn);
                //  OleDbDataAdapter oda = new OleDbDataAdapter(string.Format("select * from [{0}$]", sheetName), cnnxls);
                //  oda.Fill(tbContainer);
                //  return tbContainer;
                //dtGrid.DataSource = tbContainer;
            }
            catch (Exception)
            {
                MessageBox.Show("Error!");
                return null;
            }
        }
        #endregion
    }
}
