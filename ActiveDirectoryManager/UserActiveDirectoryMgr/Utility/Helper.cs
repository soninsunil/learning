﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace UserActiveDirectory.Utility
{
    public static class Helper
    {
        public static List<string> GetList(this string input, char saperator = ',')
        {
            List<string> list = new List<string>();
            list = input.Split(saperator).Where(a => a.Length > 0).Select(a => a.Trim()).ToList();
            return list;
        }

        public static bool ToBool(this string str)
        {
            bool.TryParse(str, out bool bVal);
            return bVal;
        }

        public static string ObjectToString(this object obj)
        {
            if (obj != null)
            {
                return obj.ToString();
            }
            else
                return string.Empty;
        }

        public static DataTable GetDataGridViewAsDataTable(this DataGridView dataGridView)
        {
            try
            {
                if (dataGridView.ColumnCount == 0) return null;

                DataTable dataTable = new DataTable();

                foreach (DataGridViewColumn col in dataGridView.Columns)
                {
                    if (col.ValueType == null)
                        dataTable.Columns.Add(col.Name, typeof(string));
                    else
                        dataTable.Columns.Add(col.Name, col.ValueType);

                    dataTable.Columns[col.Name].Caption = col.HeaderText;
                }
                foreach (DataGridViewRow row in dataGridView.Rows)
                {
                    DataRow dr = dataTable.NewRow();
                    foreach (DataColumn col in dataTable.Columns)
                    {
                        dr[col.ColumnName] = row.Cells[col.ColumnName].Value;
                    }
                    dataTable.Rows.Add(dr);
                }

                return dataTable;
            }
            catch
            {
                return null;
            }
        }
    }
}
