﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserActiveDirectory.Model
{
    public class UserAddRequest
    {
        public string UserName { get; set; }
        public string DLName { get; set; }
    }
    public class UserAddResponse
    {
        public string UserName { get; set; }
        public string ErrorMessage { get; set; }
    }
    public class ProgressDetail
    {
        public string Message { get; set; }
        public int Pct { get; set; }
    }
}
