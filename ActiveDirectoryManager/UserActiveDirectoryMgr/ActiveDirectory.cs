﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using UserActiveDirectory.ADCore;
using UserActiveDirectory.Excel;
using UserActiveDirectory.Utility;

namespace WindowsFormsApp1
{
    public partial class FormActiveDirectory : Form
    {
        public FormActiveDirectory()
        {
            InitializeComponent();
            txtDomain.Text = ConfigurationManager.AppSettings["DomainName"];
            label4.Text = string.Format("Instruction - Input DL Name to check User detail.{0}Populate User using Browse button.", Environment.NewLine);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //PopulateRows();
            // ImportfromXls();
        }

        private void ImportfromXls()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                Filter = "Excel Files (*.xls; *.xlsx; *.xlsm; *.xlsb) |*.xls; *.xlsx; *.xlsm; *.xlsb",//open file format define Excel Files(.xls)|*.xls| Excel Files(.xlsx)|*.xlsx| 
                FilterIndex = 2,

                Multiselect = false,        //not allow multiline selection at the file selection level
                Title = "Import From Excel File",   //define the name of openfileDialog
                InitialDirectory = @"Desktop" //define the initial directory
            };  //create openfileDialog Object

            if (openFileDialog1.ShowDialog() == DialogResult.OK)        //executing when file open
            {
                DataTable dataTable = Operations.ImportExcel(openFileDialog1.FileName);

                List<string> selectColumns = ConfigurationManager.AppSettings["ExcelColumnsDisplay"].GetList();

                DataTable uploadtable = dataTable.Copy();
                if (selectColumns.Count > 0)
                {
                    var removeColumns = dataTable.Columns.Cast<DataColumn>()
                       .Where(c => !selectColumns.Contains(c.ColumnName)).ToList();

                    foreach (DataColumn colToRemove in removeColumns)
                        uploadtable.Columns.Remove(colToRemove.ColumnName);
                }
                dgvCityDetails.DataSource = uploadtable;
                if (uploadtable.Rows.Count > 0)
                {
                    btnExport.Enabled = true;
                    btnGetUserDetail.Enabled = true;
                }
            }
        }

        private void BtnExport_Click(object sender, EventArgs e)
        {
            //Getting the location and file name of the excel to save from user. 
            SaveFileDialog saveDialog = new SaveFileDialog
            {
                Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
                Title = "Export Data",
                FilterIndex = 2
            };

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                Operations.ExportToExcel(dgvCityDetails, saveDialog.FileName);
                MessageBox.Show("Export Successful!");
            }
        }

        private void PopulateRows()
        {

            for (int i = 1; i <= 10; i++)
            {
                DataGridViewRow row =
                    (DataGridViewRow)dgvCityDetails.RowTemplate.Clone();

                row.CreateCells(dgvCityDetails, string.Format("City{0}", i),
                    string.Format("State{0}", i), string.Format("Country{0}", i));

                dgvCityDetails.Rows.Add(row);
            }
        }

        private void btnGetUserDetail_Click(object sender, EventArgs e)
        {
            int selectedRowCount =
                        dgvCityDetails.Rows.GetRowCount(DataGridViewElementStates.Selected);

            int rowindex = dgvCityDetails.CurrentCell.RowIndex;
            int columnindex = dgvCityDetails.CurrentCell.ColumnIndex;

            string selectedUser = dgvCityDetails.Rows[rowindex].Cells[columnindex].Value.ToString();

            //if (selectedRowCount > 0)
            //{
            //    System.Text.StringBuilder sb = new System.Text.StringBuilder();

            //    for (int i = 0; i < selectedRowCount; i++)
            //    {
            //        sb.Append("Row: ");
            //        sb.Append(dgvCityDetails.SelectedRows[i].Index.ToString());
            //        sb.Append(Environment.NewLine);
            //    }

            //    sb.Append("Total: " + selectedRowCount.ToString());
            //    MessageBox.Show(sb.ToString(), "Selected Rows");
            //}
            ActiveDirectoryCore.Instance.GetUserDetail(selectedUser);
        }

        private void btnBrowseImoprt_Click(object sender, EventArgs e)
        {
            ImportfromXls();
        }

        private void btnCheckDL_Click(object sender, EventArgs e)
        {
            if (dgvCityDetails.Rows.Count > 0 && txtDomainDLName.Text.Length > 0)
            {
                try
                {
                    string selectedUser = string.Empty;
                    int rowindex = dgvCityDetails.CurrentCell.RowIndex;
                    int columnindex = dgvCityDetails.CurrentCell.ColumnIndex;

                    selectedUser = dgvCityDetails.Rows[rowindex].Cells[columnindex].Value.ToString();

                    bool isUserActiveDL = ActiveDirectoryCore.Instance.IsUserMember(selectedUser, txtDomainDLName.Text.Trim());
                    txtResult.BackColor = Color.White;
                    txtResult.ForeColor = isUserActiveDL ? Color.Green : Color.Red;
                    txtResult.Text = $"User '{selectedUser}' Member of DL {txtDomainDLName.Text.Trim()} -> {(isUserActiveDL ? "Y" : "N")}: {DateTime.Now}";
                }
                catch (Exception ex)
                {
                    txtResult.BackColor = Color.White;
                    txtResult.ForeColor = Color.Red;
                    txtResult.Text = $"Error getting check, Message: {ex.Message}";
                }
            }
            else
            {
                txtResult.BackColor = Color.White;
                txtResult.ForeColor = Color.Red;
                txtResult.Text = $"Please select at least one User and provide Valid DL for Check. {DateTime.Now}";
            }
        }

        private void btnAddtoDL_Click(object sender, EventArgs e)
        {
            {
                if (dgvCityDetails.Rows.Count > 0 && txtDomainDLName.Text.Length > 0)
                {
                    try
                    {
                        string selectedUser = string.Empty;
                        int rowindex = dgvCityDetails.CurrentCell.RowIndex;
                        int columnindex = dgvCityDetails.CurrentCell.ColumnIndex;

                        selectedUser = dgvCityDetails.Rows[rowindex].Cells[columnindex].Value.ToString();

                        bool isUserActiveDL = ActiveDirectoryCore.Instance.IsUserMember(selectedUser, txtDomainDLName.Text.Trim());
                        if (!isUserActiveDL)
                        {
                            ActiveDirectoryCore.Instance.AddUserToGroup(selectedUser, txtDomainDLName.Text.Trim());
                            txtResult.BackColor = Color.White;
                            txtResult.ForeColor = Color.Green;
                            txtResult.Text = $"User '{selectedUser}' Added to DL {txtDomainDLName.Text.Trim()}) : {DateTime.Now.TimeOfDay}";
                        }
                        else
                        {
                            txtResult.BackColor = Color.White;
                            txtResult.ForeColor = Color.OrangeRed;
                            txtResult.Text = $"User '{selectedUser}' already Added to DL {txtDomainDLName.Text.Trim()}) : {DateTime.Now.TimeOfDay}";
                        }
                    }
                    catch (Exception ex)
                    {
                        txtResult.BackColor = Color.White;
                        txtResult.ForeColor = Color.Red;
                        txtResult.Text = $"Error getting check, Message: {ex.Message}";
                    }
                }
                else
                {
                    txtResult.BackColor = Color.White;
                    txtResult.ForeColor = Color.Red;
                    txtResult.Text = $"Please select at least one User and provide Valid DL to add User to DL.";
                }
            }
        }

        private void btnRemovefromDL_Click(object sender, EventArgs e)
        {
            if (dgvCityDetails.Rows.Count > 0 && txtDomainDLName.Text.Length > 0)
            {
                try
                {
                    string selectedUser = string.Empty;
                    int rowindex = dgvCityDetails.CurrentCell.RowIndex;
                    int columnindex = dgvCityDetails.CurrentCell.ColumnIndex;

                    selectedUser = dgvCityDetails.Rows[rowindex].Cells[columnindex].Value.ToString();

                    bool isUserActiveDL = ActiveDirectoryCore.Instance.IsUserMember(selectedUser, txtDomainDLName.Text.Trim());
                    if (isUserActiveDL)
                    {
                        ActiveDirectoryCore.Instance.RemoveUserFromGroup(selectedUser, txtDomainDLName.Text.Trim());
                        txtResult.BackColor = Color.White;
                        txtResult.ForeColor = Color.Green;
                        txtResult.Text = $"User '{selectedUser}' Removed from DL {txtDomainDLName.Text.Trim()}) : {DateTime.Now.TimeOfDay}";
                    }
                    else
                    {
                        txtResult.BackColor = Color.White;
                        txtResult.ForeColor = Color.OrangeRed;
                        txtResult.Text = $"User '{selectedUser}' is not active in DL {txtDomainDLName.Text.Trim()}) : {DateTime.Now.TimeOfDay}";
                    }
                }
                catch (Exception ex)
                {
                    txtResult.BackColor = Color.White;
                    txtResult.ForeColor = Color.Red;
                    txtResult.Text = $"Error getting check, Message: {ex.Message}";
                }
            }
            else
            {
                txtResult.BackColor = Color.White;
                txtResult.ForeColor = Color.Red;
                txtResult.Text = $"Please select at least one User and provide Valid DL to Remove User from DL.";
            }
        }
    }
}