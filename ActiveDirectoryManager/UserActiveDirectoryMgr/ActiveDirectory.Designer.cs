﻿namespace WindowsFormsApp1
{
    partial class FormActiveDirectory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code 

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor. 
        /// </summary> 
        private void InitializeComponent()
        {
            this.dgvCityDetails = new System.Windows.Forms.DataGridView();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnGetUserDetail = new System.Windows.Forms.Button();
            this.grpBoxInput = new System.Windows.Forms.GroupBox();
            this.lblDomain = new System.Windows.Forms.Label();
            this.txtDomain = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBrowseImoprt = new System.Windows.Forms.Button();
            this.grpInputs = new System.Windows.Forms.GroupBox();
            this.grpUserOperations = new System.Windows.Forms.GroupBox();
            this.txtDomainDLName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCheckDL = new System.Windows.Forms.Button();
            this.btnAddtoDL = new System.Windows.Forms.Button();
            this.btnRemovefromDL = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCityDetails)).BeginInit();
            this.grpBoxInput.SuspendLayout();
            this.grpInputs.SuspendLayout();
            this.grpUserOperations.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvCityDetails
            // 
            this.dgvCityDetails.AllowUserToAddRows = false;
            this.dgvCityDetails.AllowUserToDeleteRows = false;
            this.dgvCityDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCityDetails.Location = new System.Drawing.Point(12, 144);
            this.dgvCityDetails.MultiSelect = false;
            this.dgvCityDetails.Name = "dgvCityDetails";
            this.dgvCityDetails.ReadOnly = true;
            this.dgvCityDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCityDetails.Size = new System.Drawing.Size(877, 319);
            this.dgvCityDetails.TabIndex = 0;
            // 
            // btnExport
            // 
            this.btnExport.Enabled = false;
            this.btnExport.Location = new System.Drawing.Point(788, 474);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(101, 30);
            this.btnExport.TabIndex = 1;
            this.btnExport.Text = "&Export to Excel";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.BtnExport_Click);
            // 
            // btnGetUserDetail
            // 
            this.btnGetUserDetail.Enabled = false;
            this.btnGetUserDetail.Location = new System.Drawing.Point(647, 474);
            this.btnGetUserDetail.Name = "btnGetUserDetail";
            this.btnGetUserDetail.Size = new System.Drawing.Size(118, 30);
            this.btnGetUserDetail.TabIndex = 2;
            this.btnGetUserDetail.Text = "&Get User Detail";
            this.btnGetUserDetail.UseVisualStyleBackColor = true;
            this.btnGetUserDetail.Click += new System.EventHandler(this.btnGetUserDetail_Click);
            // 
            // grpBoxInput
            // 
            this.grpBoxInput.Controls.Add(this.txtDomain);
            this.grpBoxInput.Controls.Add(this.lblDomain);
            this.grpBoxInput.Location = new System.Drawing.Point(12, 12);
            this.grpBoxInput.Name = "grpBoxInput";
            this.grpBoxInput.Size = new System.Drawing.Size(218, 55);
            this.grpBoxInput.TabIndex = 3;
            this.grpBoxInput.TabStop = false;
            this.grpBoxInput.Text = "Active Directory Properties";
            // 
            // lblDomain
            // 
            this.lblDomain.AutoSize = true;
            this.lblDomain.Location = new System.Drawing.Point(16, 24);
            this.lblDomain.Name = "lblDomain";
            this.lblDomain.Size = new System.Drawing.Size(43, 13);
            this.lblDomain.TabIndex = 0;
            this.lblDomain.Text = "Domain";
            // 
            // txtDomain
            // 
            this.txtDomain.Location = new System.Drawing.Point(95, 21);
            this.txtDomain.Name = "txtDomain";
            this.txtDomain.ReadOnly = true;
            this.txtDomain.Size = new System.Drawing.Size(100, 20);
            this.txtDomain.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Import alcom Excel";
            // 
            // btnBrowseImoprt
            // 
            this.btnBrowseImoprt.Location = new System.Drawing.Point(131, 17);
            this.btnBrowseImoprt.Name = "btnBrowseImoprt";
            this.btnBrowseImoprt.Size = new System.Drawing.Size(64, 27);
            this.btnBrowseImoprt.TabIndex = 4;
            this.btnBrowseImoprt.Text = "&Browse";
            this.btnBrowseImoprt.UseVisualStyleBackColor = true;
            this.btnBrowseImoprt.Click += new System.EventHandler(this.btnBrowseImoprt_Click);
            // 
            // grpInputs
            // 
            this.grpInputs.Controls.Add(this.btnBrowseImoprt);
            this.grpInputs.Controls.Add(this.label1);
            this.grpInputs.Location = new System.Drawing.Point(12, 73);
            this.grpInputs.Name = "grpInputs";
            this.grpInputs.Size = new System.Drawing.Size(218, 55);
            this.grpInputs.TabIndex = 5;
            this.grpInputs.TabStop = false;
            this.grpInputs.Text = "Input";
            // 
            // grpUserOperations
            // 
            this.grpUserOperations.Controls.Add(this.btnRemovefromDL);
            this.grpUserOperations.Controls.Add(this.btnAddtoDL);
            this.grpUserOperations.Controls.Add(this.btnCheckDL);
            this.grpUserOperations.Controls.Add(this.label3);
            this.grpUserOperations.Controls.Add(this.txtDomainDLName);
            this.grpUserOperations.Location = new System.Drawing.Point(250, 12);
            this.grpUserOperations.Name = "grpUserOperations";
            this.grpUserOperations.Size = new System.Drawing.Size(351, 116);
            this.grpUserOperations.TabIndex = 6;
            this.grpUserOperations.TabStop = false;
            this.grpUserOperations.Text = "User Operations";
            // 
            // txtDomainDLName
            // 
            this.txtDomainDLName.Location = new System.Drawing.Point(83, 21);
            this.txtDomainDLName.Name = "txtDomainDLName";
            this.txtDomainDLName.Size = new System.Drawing.Size(250, 20);
            this.txtDomainDLName.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 483);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Result:-";
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(73, 480);
            this.txtResult.Name = "txtResult";
            this.txtResult.ReadOnly = true;
            this.txtResult.Size = new System.Drawing.Size(550, 20);
            this.txtResult.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "DL Name";
            // 
            // btnCheckDL
            // 
            this.btnCheckDL.Location = new System.Drawing.Point(28, 61);
            this.btnCheckDL.Name = "btnCheckDL";
            this.btnCheckDL.Size = new System.Drawing.Size(84, 37);
            this.btnCheckDL.TabIndex = 2;
            this.btnCheckDL.Text = "&Check";
            this.btnCheckDL.UseVisualStyleBackColor = true;
            this.btnCheckDL.Click += new System.EventHandler(this.btnCheckDL_Click);
            // 
            // btnAddtoDL
            // 
            this.btnAddtoDL.Location = new System.Drawing.Point(131, 61);
            this.btnAddtoDL.Name = "btnAddtoDL";
            this.btnAddtoDL.Size = new System.Drawing.Size(84, 37);
            this.btnAddtoDL.TabIndex = 3;
            this.btnAddtoDL.Text = "&ADD to DL";
            this.btnAddtoDL.UseVisualStyleBackColor = true;
            this.btnAddtoDL.Click += new System.EventHandler(this.btnAddtoDL_Click);
            // 
            // btnRemovefromDL
            // 
            this.btnRemovefromDL.Location = new System.Drawing.Point(233, 61);
            this.btnRemovefromDL.Name = "btnRemovefromDL";
            this.btnRemovefromDL.Size = new System.Drawing.Size(100, 37);
            this.btnRemovefromDL.TabIndex = 4;
            this.btnRemovefromDL.Text = "&Remove from DL";
            this.btnRemovefromDL.UseVisualStyleBackColor = true;
            this.btnRemovefromDL.Click += new System.EventHandler(this.btnRemovefromDL_Click);
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(616, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(273, 116);
            this.label4.TabIndex = 9;
            // 
            // FormActiveDirectory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 516);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.grpUserOperations);
            this.Controls.Add(this.grpInputs);
            this.Controls.Add(this.grpBoxInput);
            this.Controls.Add(this.btnGetUserDetail);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.dgvCityDetails);
            this.Name = "FormActiveDirectory";
            this.Text = "User Active Directory Management";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCityDetails)).EndInit();
            this.grpBoxInput.ResumeLayout(false);
            this.grpBoxInput.PerformLayout();
            this.grpInputs.ResumeLayout(false);
            this.grpInputs.PerformLayout();
            this.grpUserOperations.ResumeLayout(false);
            this.grpUserOperations.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCityDetails;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnGetUserDetail;
        private System.Windows.Forms.GroupBox grpBoxInput;
        private System.Windows.Forms.TextBox txtDomain;
        private System.Windows.Forms.Label lblDomain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBrowseImoprt;
        private System.Windows.Forms.GroupBox grpInputs;
        private System.Windows.Forms.GroupBox grpUserOperations;
        private System.Windows.Forms.Button btnRemovefromDL;
        private System.Windows.Forms.Button btnAddtoDL;
        private System.Windows.Forms.Button btnCheckDL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDomainDLName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.Label label4;
    }
}

