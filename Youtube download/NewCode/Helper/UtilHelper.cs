using System;

namespace Helper
{
    public static class UtilHelper
    {
        public static int ToInt(this string str)
        {
            int i;
            int.TryParse(str, out i);
            return i; 
        }
    }
}