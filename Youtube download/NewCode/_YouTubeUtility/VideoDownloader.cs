using System;
using System.IO;
using MediaToolkit;
using MediaToolkit.Model;
using VideoLibrary;
using System.Configuration;

namespace _YouTubeUtility
{
    internal class VideoDownloader
    {
        public static void SaveFile(string SaveToFolder, string VideoURL, string MP3Name)
        {
            var source = @SaveToFolder;
            var youtube = YouTube.Default;
            var vid = youtube.GetVideo(VideoURL);
            File.WriteAllBytes(Path.Combine(source, vid.FullName), vid.GetBytes());

            //var memoryStream = new MemoryStream(File.ReadAllBytes(Path.Combine(source, vid.FullName)));

            //byte[] bytes = vid.GetBytes();
            //using(MemoryStream ms = new MemoryStream())
            //{
            //    ms.Write(bytes,0,bytes.Length);
           // }
                        
            Console.WriteLine(vid.FullName);

            var inputFile = new MediaFile { Filename = Path.Combine(source, vid.FullName) };
            //string[] fullName = vid.FullName.Split(' ');
            try{
                using (var engine = new Engine())
                {
                    if(File.Exists(vid.FullName))
                    {
                    var onlyName = Path.GetFileNameWithoutExtension(vid.FullName);
                    engine.GetMetadata(inputFile);
                    engine.Convert(inputFile, 
                    new MediaFile { 
                        Filename = $"{Path.Combine(source,onlyName)}.mp3" });
                    }
                }

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                File.Delete(Path.Combine(source, vid.FullName));                
            }
       }
    }
}