﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisitAPI.Models
{
    public class Client
    {
        public string ClientName { get; set; }
        public string EmailId { get; set; }
    }
}