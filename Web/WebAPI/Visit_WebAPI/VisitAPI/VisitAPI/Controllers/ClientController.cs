﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using VisitAPI.Models;

namespace VisitAPI.Controllers
{
    /// <summary>
    /// Controller to handle client request
    /// </summary>
    public class ClientController : ApiController
    {
        Client[] clients = new Client[]
            {
                new Client{ ClientName = "Test", EmailId = "slsl@lgl.com"}
            };

        [HttpGet]
        public HttpResponseMessage Get(string id)
        {
            return new HttpResponseMessage { Content = new StringContent($"Hello from {id}"), StatusCode = HttpStatusCode.BadRequest };

        }
        /// <summary>
        /// Get client
        /// </summary>
        /// <param name="emailid">Client EMail ID</param>
        /// <returns></returns>
        [HttpGet]
        //[Route("api/v1/GetClient/{emailid}")]
        public IHttpActionResult GetClient(string emailid)
        {
            var client = clients.FirstOrDefault((p) => p.EmailId == emailid);
            if (client == null)
            {
                return NotFound();
            }
            return Ok(client);
        }

        /// <summary>
        /// Post file to server
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Post()
        {
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                var docfiles = new List<string>();
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    //var filePath = HttpContext.Current.Server.MapPath("~/" + postedFile.FileName);

                    var directoryPath = HttpContext.Current.Server.MapPath("~/");
                    directoryPath = Path.Combine(directoryPath, "ClientUploads");

                    if (!Directory.Exists(directoryPath))
                        Directory.CreateDirectory(directoryPath);


                    var filePath = Path.Combine(directoryPath, postedFile.FileName);

                    postedFile.SaveAs(filePath);
                    docfiles.Add(filePath);
                }
                result = Request.CreateResponse(HttpStatusCode.Created, docfiles);
            }
            else
            {
                result = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return result;
        }
    }
}
