﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConferenceManagement.Helper
{
    public static class Util
    {
        public static int ToInt(this string value)
        {
            int.TryParse(value, out int number);
            return number;
        }
    }
}
