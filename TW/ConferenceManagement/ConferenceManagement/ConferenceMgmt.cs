﻿using ConferenceManagement.Helper;
using ConferenceManagement.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ConferenceManagement
{
    public partial class ConferenceMgmt : Form
    {
        public ConferenceMgmt()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {

            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                Filter = "Txt Files (*.txt) |*.txt",
                FilterIndex = 1,
                Multiselect = false,        //not allow multiline selection at the file selection level
                Title = "Import txt File",   //define the name of openfileDialog
                InitialDirectory = @"Desktop" //define the initial directory
            };  //create openfileDialog Object

            if (openFileDialog1.ShowDialog() == DialogResult.OK)        //executing when file open
            {
                var path = openFileDialog1.FileName;

                var fileData = File.ReadAllLines(@"D:\Learning\Test\EventDetails.txt").ToList();

                List<EventOutput> outputData = new List<EventOutput>();

                List<Talk> input = new List<Talk>();
                foreach (var line in fileData)
                {
                    Regex durationRegex = new Regex(@"[\d]+min");
                    var time = durationRegex.Match(line);
                    TimeSpan duration;
                    if (time.Success)
                    {
                        var topicDuration = time.Value;
                        duration = new TimeSpan(0, topicDuration.Replace("min", "").ToInt(), 0);
                    }
                    else if (line.Contains("lightning"))
                    {
                        duration = new TimeSpan(0, 5, 0);
                    }
                    else
                    {
                        //Not a valid event skip from the schedule
                        duration = new TimeSpan(0, 0, 0);
                    }
                    if (duration.Ticks > 0)
                    {
                        input.Add(new Talk(line, duration));
                    }
                }

                var sortedList = input.OrderByDescending(a => a.Duration).ToList();

                int i = 0;
                while (sortedList.Count > i)
                {
                    List<string> outputDayEvent = new List<string>();

                    DateTime dummyDayStartDate = DateTime.Now.Date;
                    DateTime dummyDayStartdateTime = dummyDayStartDate + new TimeSpan(9, 0, 0);
                    DateTime dummyDayEnddateTime = dummyDayStartDate + new TimeSpan(17, 0, 0);

                    do
                    {
                        if (i == sortedList.Count)
                        {
                            break;
                        }
                        if (dummyDayStartdateTime.TimeOfDay >= new TimeSpan(12, 0, 0) && dummyDayStartdateTime.TimeOfDay < new TimeSpan(13, 0, 0))
                        {
                            //lunch time
                            outputDayEvent.Add($"{dummyDayStartdateTime:hh:mmtt} Lunch");
                            dummyDayStartdateTime += new TimeSpan(1, 0, 0);

                        }
                        else if (dummyDayStartdateTime.TimeOfDay >= new TimeSpan(16, 0, 0))
                        {
                            outputDayEvent.Add($"{dummyDayStartdateTime:hh:mmtt} Networking Event");
                            dummyDayStartdateTime += new TimeSpan(1, 0, 0);
                        }
                        else
                        {
                            outputDayEvent.Add($"{dummyDayStartdateTime:hh:mmtt} {sortedList[i].Topic}");
                            dummyDayStartdateTime += sortedList[i].Duration;
                        }
                        i++;

                    }
                    while (dummyDayEnddateTime > dummyDayStartdateTime);

                    outputData.Add(new EventOutput { Data = outputDayEvent });
                }

            }
            MessageBox.Show("Lets Proceed.");
        }
    }
}