﻿using System;

namespace ConferenceTimeManament.Helper
{
    public static class Util
    {
        public static int ToInt(this string value)
        {
            try
            {
                return int.Parse(value);
            }
            catch (ArgumentException e)
            {
                throw;
            }
        }
    }
}
