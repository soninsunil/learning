﻿using ConferenceTimeManament.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConferenceTimeManament.Core
{
    public static class Processor
    {
        public static List<EventOutput> Process(List<Talk> talks)
        {
            List<EventOutput> outputData = new List<EventOutput>();
            int i = 0;
            int trackCount = 1;
            while (talks.Count > i)
            {
                List<string> outputDayEvent = new List<string>();

                DateTime dummyDayStartDate = DateTime.Now.Date;
                DateTime dummyDayStartdateTime = dummyDayStartDate + new TimeSpan(9, 0, 0);
                DateTime dummyDayEnddateTime = dummyDayStartDate + new TimeSpan(17, 0, 0);

                do
                {
                    if (i == talks.Count)
                    {
                        break;
                    }
                    if (dummyDayStartdateTime.TimeOfDay >= new TimeSpan(12, 0, 0) && dummyDayStartdateTime.TimeOfDay < new TimeSpan(13, 0, 0))
                    {
                        //lunch time
                        outputDayEvent.Add($"{dummyDayStartdateTime:hh:mmtt} Lunch");
                        dummyDayStartdateTime += new TimeSpan(1, 0, 0);

                    }
                    else if (dummyDayStartdateTime.TimeOfDay >= new TimeSpan(16, 0, 0))
                    {
                        outputDayEvent.Add($"{dummyDayStartdateTime:hh:mmtt} Networking Event");
                        dummyDayStartdateTime += new TimeSpan(1, 0, 0);
                    }
                    else
                    {
                        outputDayEvent.Add($"{dummyDayStartdateTime:hh:mmtt} {talks[i].Topic}");
                        dummyDayStartdateTime += talks[i].Duration;
                    }
                    i++;

                }
                while (dummyDayEnddateTime > dummyDayStartdateTime);

                outputDayEvent.Insert(0, $"Track {trackCount}");
                trackCount++;

                outputData.Add(new EventOutput {Data = outputDayEvent });
            }
            return outputData;
        }
    }
}
