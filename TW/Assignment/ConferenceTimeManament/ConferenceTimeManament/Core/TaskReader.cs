﻿using ConferenceTimeManament.Helper;
using ConferenceTimeManament.Model;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ConferenceTimeManament.Core
{
    public interface IReadTask
    {
        List<Talk> ReadInput();
    }
    public class TaskReader : IReadTask
    {
        public List<string> TalksList { get; private set; }

        public TaskReader(List<string> inputFileData)
        {
            TalksList = new List<string>();
            TalksList = inputFileData;
        }
        
        public List<Talk> ReadInput()
        {
            var inputTalks = new List<Talk>();

            try
            {
                foreach (var line in TalksList)
                {
                    Regex durationRegex = new Regex(@"[\d]+min");
                    var time = durationRegex.Match(line);
                    TimeSpan duration;
                    if (time.Success)
                    {
                        var topicDuration = time.Value;
                        duration = new TimeSpan(0, topicDuration.Replace("min", "").ToInt(), 0);
                    }
                    else if (line.Contains("lightning"))
                    {
                        duration = new TimeSpan(0, 5, 0);
                    }
                    else
                    {
                        //Not a valid event skip from the schedule
                        duration = new TimeSpan(0, 0, 0);
                    }
                    if (duration.Ticks > 0)
                    {
                        inputTalks.Add(new Talk(line, duration));
                    }
                    else
                        throw new Exception("Time cannot be -ve for any input");
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return inputTalks;
        }
    }
}
