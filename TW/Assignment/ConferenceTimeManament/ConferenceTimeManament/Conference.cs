﻿using ConferenceTimeManament.Core;
using ConferenceTimeManament.Model;
using System.Collections.Generic;
using System.Linq;

namespace ConferenceTimeManament
{
    public class Conference
    {
        public IReadTask ReadTask { get; private set; }

        public Conference(IReadTask readTask)
        {
            ReadTask = readTask;
        }

        public List<EventOutput> ProcessTask()
        {
            List<Talk> talks = ReadTask.ReadInput();
            var sortedList = talks.OrderByDescending(a => a.Duration).ToList();
            return Processor.Process(sortedList);
        }
    }
}
