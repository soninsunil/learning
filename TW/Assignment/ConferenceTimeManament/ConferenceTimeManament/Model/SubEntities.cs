﻿using System;
using System.Collections.Generic;

namespace ConferenceTimeManament.Model
{
    public class Talk
    {
        public TimeSpan Duration { get; private set; }
        public string Topic { get; private set; }

        public Talk(string topic, TimeSpan timeDuration)
        {
            try
            {
                if (timeDuration.Ticks < 0)
                {
                    throw new ArgumentException("Invalid Time Value");
                }
                Duration = timeDuration;
                Topic = topic;
            }
            catch (ArgumentException e)
            {
                throw;
            }
        }
    }
    public class EventOutput
    {
        public List<string> Data { get; set; }
    }
}
