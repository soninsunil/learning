﻿using ConferenceTimeManament;
using ConferenceTimeManament.Core;
using System;
using System.Configuration;
using System.IO;
using System.Linq;

namespace ConferenceTimeManagerExe
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = ConfigurationManager.AppSettings["TextFilePath"];
            if (File.Exists(filePath) && !string.IsNullOrEmpty(filePath))
            {
                var fileData = File.ReadAllLines(filePath).ToList();
                IReadTask readTask = new TaskReader(fileData);
                Conference conference = new Conference(readTask);
                var scheduledTalks = conference.ProcessTask();

                foreach (var session in scheduledTalks)
                {
                    foreach (var task in session.Data)
                    {
                        Console.WriteLine(task);
                    }
                }
            }
            else
            {
                Console.WriteLine("Input TXT file not found, please make sure file is present at location as configured from Config file. Sample is available at Input folder.");
            }
            //Console.WriteLine("Task Completed.");
            Console.ReadLine();
        }
    }
}
