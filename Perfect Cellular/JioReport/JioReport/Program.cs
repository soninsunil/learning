﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.Data;
using ExcelDataReader;
using System.Text.RegularExpressions;
using OfficeOpenXml;

namespace JioReport
{
    public static class Util
    {
        public static decimal ToDecimal(this string str)
        {
            decimal.TryParse(str, out decimal d);
            return d;
        }
        public static bool IsContains(this string source, string toCheck, StringComparison comp)
        {
            return source?.IndexOf(toCheck, comp) >= 0;
        }
    }
    public class ExtractData
    {
        public string Number { get; set; }
        public DateTime DataDate { get; set; }
        public string TransactionID { get; set; }
        public decimal CurrentBalance { get; set; }
        public decimal UpdatedBalance { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var DirName = ConfigurationManager.AppSettings["DirectoryPath"];
            var FileName = ConfigurationManager.AppSettings["FileName"];
            try
            {
                var filepath = Path.Combine(DirName, FileName);
                Console.WriteLine($"Started reading xls from Path - {filepath}");
                var excelData = ReadExcel(filepath);

                var dataRows = excelData.Select("[Address] like '%JIO%'");

                List<ExtractData> extracts = new List<ExtractData>();

                foreach (var row in dataRows)
                {
                    var data = row.ItemArray[5].ToString().Contains("transaction") ? row.ItemArray[5].ToString() : "";
                    if (string.IsNullOrEmpty(data))
                        data = row.ItemArray[5].ToString().Contains("Transaction") ? row.ItemArray[5].ToString() : "";

                    if (!string.IsNullOrEmpty(data))
                    {
                        var amounts = Regex.Split(data, @"[^0-9\.]+");
                        var amount = amounts.Where(a => a.Split('.').Length > 1 && a.Split('.')[1].Length > 0).ToList();
                        var balance = amount.Where(a => a.Length > 1).ToList();

                        var transaction = new Regex("[A-Z]{2}[0-9]{4}[0-9A-Z]{6}");
                        Match match = transaction.Match(data);
                        if (match.Success)
                        {
                            ExtractData extract = new ExtractData();
                            extract.TransactionID = match.Value;
                            extract.Number = row.ItemArray[1].ToString();
                            extract.DataDate = DateTime.ParseExact(row.ItemArray[3].ToString(), "yyyy-MM-dd ddd HH:mm:ss", null);
                            try
                            {
                                extract.CurrentBalance = balance[0].ToString().TrimEnd('.').ToDecimal();

                            }
                            catch (Exception)
                            {

                            }
                            if (balance.Count > 1)
                            {
                                extract.UpdatedBalance = balance.Count == 2 ? balance[1].ToString().TrimEnd('.').ToDecimal() : 0;
                            }

                            extracts.Add(extract);
                        }
                    }
                
                }
                var outputFileName = $"Output_{DateTime.Now:ddMMyyyyHHmmss}.xlsx";
                var exportFilePath = Path.Combine(DirName, outputFileName);
                ExportToExcel(extracts, exportFilePath);
                Console.WriteLine($"File Exported to path - {Path.Combine(DirName, outputFileName)}");
            }
            catch (Exception ee)
            {
                Console.WriteLine($"Error while executing program: {ee.Message}");
            }
            finally
            {
                Console.WriteLine($"completed.. Press any key to Exit.");
                Console.ReadLine();
            }
        }

        private static void ExportToExcel(List<ExtractData> extracts, string exportFilePath)
        {
            FileInfo fi = new FileInfo(exportFilePath);
            using (ExcelPackage package = new ExcelPackage(fi))
            {
                ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.Add("MobileReport");
                excelWorksheet.Cells["A1"].LoadFromCollection(extracts, true);

                using (ExcelRange col = excelWorksheet.Cells[$"B1:B{extracts.Count + 1}"])
                {
                    col.Style.Numberformat.Format = "dd-MMM-yyyy HH:mm:ss";
                }

                using (ExcelRange col = excelWorksheet.Cells[$"D1:D{extracts.Count + 1}"])
                {
                    col.Style.Numberformat.Format = "₹ #,##0.00";
                }

                using (ExcelRange col = excelWorksheet.Cells[$"E1:E{extracts.Count + 1}"])
                {
                    col.Style.Numberformat.Format = "₹ #,##0.00";
                }
                package.Save();
            }
        }

        private static DataTable ReadExcel(string filepath, string sheetName = "")
        {
            FileStream fs = File.Open(filepath, FileMode.Open, FileAccess.Read);

            IExcelDataReader excelDataReader = Path.GetExtension(filepath).ToUpper() == ".XLS" ?
                ExcelReaderFactory.CreateBinaryReader(fs) : ExcelReaderFactory.CreateOpenXmlReader(fs);

            DataSet ds;

            using (excelDataReader)
            {
                ds = excelDataReader.AsDataSet(new ExcelDataSetConfiguration
                {
                    UseColumnDataType = false,
                    ConfigureDataTable = (tr) => new ExcelDataTableConfiguration { UseHeaderRow = true }
                });
            }
            return string.IsNullOrEmpty(sheetName) ? ds.Tables[0] : ds.Tables[sheetName];
        }
    }
}
