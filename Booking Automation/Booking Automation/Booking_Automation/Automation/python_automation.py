from selenium import webdriver
from selenium.webdriver.common.keys import Keys

import selenium.webdriver.support.ui as ui

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import sys,os

para =len(sys.argv) - 1 
print("Total Parameter: ", para)

if para < 3:
  print("3 Parameter excepted from the command line. 1 - UserName, 2 - Password, 3 - DeskId")
  sys.exit(1)

#print(sys.argv[0])

user = sys.argv[1]
pwd = sys.argv[2]
DeskID = sys.argv[3]

#user = "kaushik.kantipal@thomsonreuters.com"
print("UserName: ",user)
#pwd = "Bristy1980.."
print("Password: ",pwd)
#DeskID = "item2327"
print("DeskID: ",DeskID)

absFilePath = os.path.abspath(os.path.dirname(sys.argv[0]))
print(absFilePath)
driverPath = absFilePath + "\\gecko\\geckodriver.exe"
print(driverPath)


driver = webdriver.Firefox(executable_path=driverPath)
driver.set_page_load_timeout(50)
driver.get("https://thomsonreuters.condecosoftware.com")

#elem = driver.find_element_by_id("btnRedirectID")
#elem.send_keys(Keys.RETURN)

print("Waiting at login page...")
driver.implicitly_wait(15)

elem = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.ID, "userNameInput")))
elem.send_keys(user)

elem = driver.find_element_by_id("passwordInput")
elem.send_keys(pwd)
elem.send_keys(Keys.RETURN)
print("Log In Successful...")

elem = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.LINK_TEXT, "Go to app")))

elem.send_keys(Keys.RETURN)

#elem = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.ID, "RoomBookingMenuOptions")))
#elem.send_keys(Keys.RETURN)


driver.switch_to.frame(WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.ID, "mainDisplayFrame"))))
print("Getting the Booking Details...")

WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.ID,"startDateTextBox"))).click()

#TODO: Add logic to increment date by 3 days
def getNextDate(selectedDate):
    (t,a,b,c) = selectedDate.split("_")
    y = int(c) + 3
    if y > 6:
      x = int(b) + 1
      y = y - 7
      print(str(y))
    else:
      x = int(b)
    newstr = "_".join((t,a,str(x), str(y)))
    return newstr

elem = driver.find_element_by_xpath('//td[@class="ajax__calendar_active"]/div[1]')
print("Selected Date ID of active element: ", elem.get_attribute("id"))
print("Selected Date ClassName of active element: ", elem.get_attribute("class"))
print("Selected Date Title of active element: ", elem.get_attribute("title"))

strold = elem.get_attribute("id")
print("Calling function to get new id for next date input parameter: ",strold)
strnew = getNextDate(strold)
print("Returned new attribute id after 3 dates is: ", strnew)

elem = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.ID,strnew)))
print("Attribute id of found element: ", elem.get_attribute("id"))
print("Attribute class of found element: ", elem.get_attribute("class"))
print("Attribute title of found element: ", elem.get_attribute("title"))
elem.click()

elem = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.ID,"FloorPlanButton")))
elem.send_keys(Keys.RETURN)

driver.switch_to.frame(WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.ID,"iframeFloorPlan"))))
elem = driver.find_element_by_xpath('//div[@id="item1082" and @class="groupselector"]/a[1]')
elem.send_keys(Keys.RETURN)

driver.implicitly_wait(15)

#"item2264"
print("Getting element with DeskID: ", DeskID)
elem = driver.find_element_by_xpath('//div[@id="'+DeskID+'" and @class="DeskMainLayer"]/div[1]/div[1]')
#print(elem.get_attribute("class"))

parent_h = driver.current_window_handle[0]
#print(parent_h)

print("Booking Confirmation Page....")

driver.execute_script("arguments[0].click();", elem)

window_after = driver.window_handles[1]
driver.switch_to.window(window_after)
str2=driver.title
#print(str2)
#print(window_after)

driver.implicitly_wait(10)
elem = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.ID,"submitButton")))
#print(elem.get_attribute("class"))
elem.send_keys(Keys.RETURN)

print("Booking Completed....")

wait = WebDriverWait(driver, 50).until(EC.alert_is_present())

alert = driver.switch_to.alert
alert.accept()

print("Exiting Firefox Browser....")

driver.quit()
