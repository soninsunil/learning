﻿/*
// Sample code to perform I/O:

name = Console.ReadLine();                  // Reading input from STDIN
Console.WriteLine("Hi, {0}.", name);        // Writing output to STDOUT

// Warning: Printing unwanted or ill-formatted data to output will cause the test cases to fail
*/

// Write your code here
using System;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] arry = { { 3, 6, 9 }, { 4, 8, 12 }, { 5, 10, 15 } };
            Console.WriteLine("Entered Matrix");
            Matrix mtx = new Matrix();
            mtx.DisplayMatrix(arry, 3, 3);

            mtx.Degree90Matrix(arry, 3, 3);
            mtx.rotate180Matrix(arry, 3, 3);
            //Console.ReadLine();
            Console.ReadLine();
        }
    }
    class Matrix
    {
        public void DisplayMatrix(int[,] arry, int m, int n)
        {
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write(arry[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
        public void Degree90Matrix(int[,] arry, int m, int n)
        {
            int j = 0;
            int p = 0;
            int q = 0;
            int i = m - 1;
            int[,] rotatedArr = new int[m, n];

            //for (int i = m-1; i >= 0; i--)
            for (int k = 0; k < m; k++)
            {

                while (i >= 0)
                {
                    rotatedArr[p, q] = arry[i, j];
                    q++;
                    i--;
                }
                j++;
                i = m - 1;
                q = 0;
                p++;

            }
            DisplayMatrix(rotatedArr, m, n);

        }

        public void rotate180Matrix(int[,] mat, int m, int n)
        {
            int N = 3;
            int[,] rotatedArr = new int[m, n];
            int p = 0;
            int q = 0;
            // Simply print from last 
            // cell to first cell. 
            for (int i = N - 1; i >= 0; i--)
            {
                q = 0;
                for (int j = N - 1; j >= 0; j--)
                {

                    rotatedArr[p, q] = mat[i, j];
                    q++;
                }
                p++;
                   // Console.Write(mat[i, j] + " ");

               // Console.WriteLine();
            }
            DisplayMatrix(rotatedArr, m, n);
        }
    }
}