﻿using System;
using Core;

namespace HackerRank
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Excercise Started!");
            //Grading - https://www.hackerrank.com/challenges/grading/problem
            int[] input =new int[4]{ 73, 67, 38, 33};
            var returnVal = Sample.gradingStudents(input);
            foreach(var val in returnVal)
            {
                Console.WriteLine(val);
            }
            Console.ReadLine();
            Console.WriteLine("Done.");
        }
    }
}