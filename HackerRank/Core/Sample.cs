using System;
using System.Collections;
using System.Collections.Generic;

namespace Core
{
    public static class Sample
    {

        public static int[] gradingStudents(int[] grades)
        {
            List<int> inVal = new List<int>();
            foreach(int i in grades)
            {
                int q = i / 5;
                int nextNearNumber = ((q+1) * 5);
                if(nextNearNumber >= 40)
                {
                    if(( nextNearNumber - i) < 3)
                        inVal.Add(nextNearNumber);
                    else
                        inVal.Add(i);
                }
                else
                    inVal.Add(i); 
            }
            return inVal.ToArray();
        }
    }
}